import models.Movable;
import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(0, 0); 
        App.testMovableObject(point1);

        MovablePoint point2 = new MovablePoint(5, 4);
        App.testMovableObject(point2);
    } 

    public static void testMovableObject(Movable movableObject) {
        movableObject.moveUp();
        System.out.println(movableObject.toString());

        movableObject.moveDown();
        System.out.println(movableObject.toString());

        movableObject.moveLeft();
        System.out.println(movableObject.toString());

        movableObject.moveRight();
        System.out.println(movableObject.toString());
    }
}
