package models;

public class MovablePoint implements Movable {
    private Integer x;
    private Integer y;

    public MovablePoint(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "MovablePoint [x=" + x + ", y=" + y + "]";
    }

    @Override
    public void moveDown() {
        this.y--;
    }

    @Override
    public void moveLeft() {
        this.x--;
        
    }

    @Override
    public void moveRight() {
        this.x++;
        
    }

    @Override
    public void moveUp() {
        this.y++;    
    }
}
